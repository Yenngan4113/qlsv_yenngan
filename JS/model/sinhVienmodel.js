function SinhVien(_maSV, _tenSV, _email, _toan, _ly, _hoa) {
  this.maSV = _maSV;
  this.tenSV = _tenSV;
  this.email = _email;
  this.toan = _toan;
  this.ly = _ly;
  this.hoa = _hoa;
  this.tinhDTB = function () {
    return (this.toan + this.hoa + this.ly) / 3;
  };
}

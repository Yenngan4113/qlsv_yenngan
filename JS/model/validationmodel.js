function ValidationSV() {
  this.kiemTraRong = function (idcheck, iderror, errormessage) {
    let content = document.querySelector(idcheck).value.trim();
    if (content == "") {
      document.querySelector(iderror).innerText = errormessage;
      return false;
    } else {
      document.querySelector(iderror).innerText = "";
      return true;
    }
  };
  this.checkIDHopLe = function () {
    let maSV = document.querySelector("#txtMaSV").value.trim();
    let index = searchIDPosition(maSV, danhSachSV);
    if (index == -1) {
      return true;
    } else {
      document.querySelector("#spanMaSV").innerText = "Không được trùng mã SV";
    }
  };
  this.checkEmailHopLe = function () {
    let email = document.querySelector("#txtEmail").value.trim();
    let parten = /^[a-z0-9](.?[a-z0-9]){5,}@g(oogle)?mail.com$/;
    if (parten.test(email)) {
      document.querySelector("#spanEmailSV").innerText = "";

      return true;
    } else {
      document.querySelector("#spanEmailSV").innerText = "Email không hợp lệ";
    }
  };
  this.checkNumbers = function (idcheck, iderror, errormessage) {
    let score = document.querySelector(idcheck).value;
    let numbers = /^[0-9]+$/;
    if (score.match(numbers) && score >= 0 && score <= 10) {
      document.querySelector(iderror).innerText = "";
      return true;
    } else {
      document.querySelector(iderror).innerText = errormessage;
      return false;
    }
  };
  this.checkLetters = function (idcheck, iderror, errormessage) {
    let studentName = document.querySelector(idcheck).value;
    let letters =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (letters.test(studentName)) {
      document.querySelector(iderror).innerText = "";
      return true;
    } else {
      document.querySelector(iderror).innerText = errormessage;
      return false;
    }
  };
}

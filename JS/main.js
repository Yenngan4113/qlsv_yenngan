let danhSachSV = [];
let validationSV = new ValidationSV();
let DSSV_STORAGE = "DSSV_STORAGE";
const saveLocalStorage = function () {
  danhSachSVJson = JSON.stringify(danhSachSV);
  localStorage.setItem(DSSV_STORAGE, danhSachSVJson);
};
const getLocalStorage = function () {
  let danhSachSVJson = localStorage.getItem(DSSV_STORAGE);
  if (danhSachSVJson) {
    danhSachSV = JSON.parse(danhSachSVJson);
    danhSachSV = danhSachSV.map(function (item) {
      return new SinhVien(
        item.maSV,
        item.tenSV,
        item.email,
        item.toan,
        item.ly,
        item.hoa
      );
    });
  }
};
getLocalStorage();
showSVlist(danhSachSV);
function searchIDPosition(id, array) {
  return array.findIndex(function (item) {
    return item.maSV == id;
  });
}

let isUpdate = false;

let checkValidate = function () {
  let isValidEmail = validationSV.checkEmailHopLe();
  let isValidNumber =
    validationSV.checkNumbers(
      "#txtDiemToan",
      "#spanToan",
      "Điểm không hợp lệ"
    ) &
    validationSV.checkNumbers("#txtDiemLy", "#spanLy", "Điểm không hợp lệ") &
    validationSV.checkNumbers("#txtDiemHoa", "#spanHoa", "Điểm không hợp lệ");
  let isValidLetter = validationSV.checkLetters(
    "#txtTenSV",
    "#spanTenSV",
    "Tên Sinh Viên không hợp lệ"
  );
  //let isValid = isValidMaSV && isValidEmail && isValidNumber && isValidLetter;
  if (isUpdate) {
    return (isValid = isValidEmail && isValidNumber && isValidLetter);
  } else {
    let isValidMaSV =
      validationSV.kiemTraRong(
        "#txtMaSV",
        "#spanMaSV",
        "Không được để trống mã Sinh Viên"
      ) && validationSV.checkIDHopLe();
    return (isValid =
      isValidMaSV && isValidEmail && isValidNumber && isValidLetter);
  }
};
function themSinhVien() {
  document.querySelector("#txtMaSV").disabled = false;
  isUpdate = false;
  checkValidate();
  console.log(isValid);
  if (isValid) {
    let newSinhVien = getInforFromInputForm();
    danhSachSV.push(newSinhVien);
    showSVlist(danhSachSV);
    saveLocalStorage();
  }
}
function deleteSV(id) {
  let position = searchIDPosition(id, danhSachSV);
  danhSachSV.splice(position, 1);
  showSVlist(danhSachSV);
  saveLocalStorage();
}
function editSV(id) {
  document.querySelector("#txtMaSV").disabled = true;
  document.querySelector("#spanMaSV").innerText = "";
  let position = searchIDPosition(id, danhSachSV);
  let sinhVienedit = danhSachSV[position];
  showSVedit(sinhVienedit);
}
function updateSV() {
  isUpdate = true;
  checkValidate();
  if (isValid) {
    let sinhVienedited = getInforFromInputForm();
    let position = searchIDPosition(sinhVienedited.maSV, danhSachSV);
    if (position != -1) {
      danhSachSV[position] = sinhVienedited;
      showSVlist(danhSachSV);
      document.querySelector("#spanMaSV").innerText = "";
    } else {
      document.querySelector("#spanMaSV").innerText =
        "Không được thay đổi Mã Sinh Viên";
    }
    saveLocalStorage();
  }
}
function resetSV() {
  document.getElementById("txtMaSV").disabled = false;
  document.querySelector("#txtMaSV").value = "";
  document.querySelector("#txtTenSV").value = "";
  document.querySelector("#txtEmail").value = "";
  document.querySelector("#txtDiemToan").value = "";
  document.querySelector("#txtDiemLy").value = "";
  document.querySelector("#txtDiemHoa").value = "";
}
document.querySelector("#btnSearch").addEventListener("click", function () {
  let dsSearchSV = [];
  let searchSVName = document.querySelector("#txtSearch").value;
  console.log(searchSVName);
  danhSachSV.forEach(function (item) {
    console.log(item.tenSV);
    if (item.tenSV == searchSVName) {
      dsSearchSV.push(item);
    }
  });
  if (dsSearchSV != "") {
    showSVlist(dsSearchSV);
    document.querySelector("#searchErr").innerText = "";
  } else {
    showSVlist(dsSearchSV);
    document.querySelector("#searchErr").innerText = "Không tìm thấy Sinh Viên";
  }
});

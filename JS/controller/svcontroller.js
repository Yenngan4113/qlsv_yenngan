function getInforFromInputForm() {
  let maSV = document.querySelector("#txtMaSV").value;
  let tenSV = document.querySelector("#txtTenSV").value;
  let email = document.querySelector("#txtEmail").value;
  let diemToan = +document.querySelector("#txtDiemToan").value;
  let diemLy = +document.querySelector("#txtDiemLy").value;
  let diemHoa = +document.querySelector("#txtDiemHoa").value;
  let sinhVien = new SinhVien(maSV, tenSV, email, diemToan, diemLy, diemHoa);
  return sinhVien;
}
function showSVlist(dssv) {
  let contentHTML = "";
  for (let i = 0; i < dssv.length; i++) {
    let sinhVien = dssv[i];
    let contentTrtag = /*html*/ `<tr>
    <td>${sinhVien.maSV}</td>
    <td>${sinhVien.tenSV}</td>
    <td>${sinhVien.email}</td>
    <td>${sinhVien.tinhDTB()}</td>
    <td>
    <button class="btn btn-success edit" onClick="editSV('${
      sinhVien.maSV
    }')">Sửa</button>
    <button class="btn btn-danger delete" onClick="deleteSV('${
      sinhVien.maSV
    }')">Xóa</button></td>
    </tr>`;
    contentHTML += contentTrtag;
  }
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
}
function showSVedit(sv) {
  document.querySelector("#txtMaSV").value = sv.maSV;
  document.querySelector("#txtTenSV").value = sv.tenSV;
  document.querySelector("#txtEmail").value = sv.email;
  document.querySelector("#txtDiemToan").value = sv.toan;
  document.querySelector("#txtDiemLy").value = sv.ly;
  document.querySelector("#txtDiemHoa").value = sv.hoa;
}
